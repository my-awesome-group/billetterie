<?php
	require('model/Billet.php');

	if(!isset($_POST["reservation"]) || empty($_POST["reservation"]))
   	 	{
			include_once('vue/billet.php');
   	 	}
   	else {
   	 		$listeBillet = $_POST["reservation"];
   	 		if($billet->verifReservation()){
   	 			$billet->reserverBillet($listeBillet);
   	 			include_once('vue/billetValide.php');
   	 		}
   	 		else {
   	 			include_once('vue/billetEchoue.php');
   	 		}
   	}
?>