

<header>
	<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">
						<img id="navLogo" src="./images/LOGO-OPENPARCATP.png" alt="Logo" width="200">
					</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Accueil</a></li>
					<li><a href="#">Infos Tournoi</a></li>
					<li><a href="#">Billeterie</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
					<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Connexion</a></li>
				  </ul>
				</div>
		</div >
	  </nav>
	 <div class="page-header text-center">
		 <div class="paralcontainer-wrap">
			<div class="paralcontainer">
				<h1>Open Parc de Lyon</h1>
				<h2>4éme Edition</h2>
				<p class="lead"><strong>Du 16 au 23 Mai 2020</strong></p>
				<p class="lead"><em>PARC DE LA TÊTE D'OR - LYON</em></p>
			 </div>
		 </div> 
	 </div>
</header>

<section id="menu">
 	<div class="container-fluid">
 		<div class="container text-center">
	 		<div class="row">
	 			<div class="col-sm-4">
	 				<a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-calendar"></span> Planning</a>
	 			</div>
	 			<div class="col-sm-4">
	 				<a href="<?= "index.php?page=billet"?>" class="btn btn-warning"><span class="glyphicon glyphicon-shopping-cart"></span> Billeterie</a>
	 			</div>
	 			<div class="col-sm-4">
	 				<a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-user"></span> Zone VIP</a>
	 			</div>
	 		</div>
	 		<div class="actu">
	 			<a href="#" class="btn btn-warning actu"><span class="glyphicon glyphicon-list-alt"></span> Actualités</a>
	 		</div>
 		</div>
 	</div>
</section>
